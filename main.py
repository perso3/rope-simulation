import pygame
from pygame.locals import *
import time
from BasicVector import Vec2

from tentacle import Tentacle
from renderer import render_all


def simulate():
    WINDOW_SIZE = (600, 600)

    pygame.init()

    framerate = 60
    time_per_frame = 1 / framerate
    last_time = time.time()
    frameCount = 0

    mouse_pos = None
    pause = False

    tentacles = []
    for i in range(400):
        tentacles.append(Tentacle(Vec2(100, 100), Vec2(101, 100)))

    window = pygame.display.set_mode(WINDOW_SIZE)
    pygame.display.set_caption("")
    mainClock = pygame.time.Clock()

    simulation = True

    while simulation:
        delta_time = time.time() - last_time
        while delta_time < time_per_frame:
            delta_time = time.time() - last_time

            for event in pygame.event.get():

                if event.type == QUIT:
                    simulation = False

                if event.type == MOUSEMOTION:
                    mouse_pos = event.pos
                    # print(mouse_pos)

                if event.type == KEYDOWN:
                    if event.key == 32:
                        pause = not pause
                    elif event.key == 1073741886:
                        pass

                if event.type == MOUSEBUTTONDOWN:
                    pass

        if not pause:
            pass

        if frameCount % 1 == 0:
            if mouse_pos is not None:
                new_pos = Vec2(mouse_pos[0], mouse_pos[1])
                for tentacle in tentacles:
                    tentacle.update(new_pos)
                    new_pos = tentacle.end
                render_all(window, tentacles)

        pygame.display.update()

        last_time = time.time()
        frameCount += 1


if __name__ == '__main__':
    simulate()

