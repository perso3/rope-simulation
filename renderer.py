import pygame.draw


def clear(window):
    window.fill((0, 0, 0))


def render_all(window, tentacles):
    clear(window)
    for tentacle in tentacles:

        start = tentacle.start.getPos()
        end = tentacle.end.getPos()

        line_color = (255, 0, 0)

        pygame.draw.line(window, line_color, start, end)
