from BasicVector import Vec2


class Tentacle:
    def __init__(self, start, end):
        self.start = start
        self.end = end
        self.length = Vec2.dist(start, end)

    def update(self, new_pos):
        self.start = new_pos
        dist_vec = new_pos - self.end
        dist_vec.normalize()
        self.end = self.start - dist_vec * self.length